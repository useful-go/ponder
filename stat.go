package ponder

import (
	"time"
)

type Mode uint32

const (
	// ModeSetuid can modify the users of this document.
	ModeSetuid Mode = 0o4000
	// ModeSetgid can modify the groups of this document.
	ModeSetgid Mode = 0o2000
	// ModeSticky documents cannot be deleted by non-owners if set in Doc.Mode.
	ModeSticky Mode = 0o1000
	// ModeRead read the document
	ModeRead Mode = 0o004
	// ModeWrite can write the document
	ModeWrite Mode = 0o002
	// ModeList can list the document
	ModeList Mode = 0o001
)

// ModeMap is map from ID to mode
type ModeMap map[ID]Mode

// DocInfo keeps track of metadata of a ponder document.
// This includes the permissionsm users and groups of the document.
// It also keeps track of time stamps and hidden status of documents.
type DocInfo struct {
	// ID of the document that this DocInfo applies to.
	// This is also used as the ID of the DocInfo itself
	// The ID should be a globally unique ID such as an UUID, ULID or URL.
	ID ID `json:"id" rethinkdb:"id"`

	// Mode of this document for users who are not a oner in users or groups.
	// Users and groups may have their own modes.
	Mode Mode `json:"mode" rethinkdb:"mode"`

	// Users are the IDs of the users who own this document mapped
	// to their Mode permissions.
	Users ModeMap `json:"users" rethinkdb:"users"`

	// Groups are the IDs of the groups who own this document mapped
	// to their Mode permissions.
	Groups ModeMap `json:"groups" rethinkdb:"groups"`

	// Mod is the modification time of the document.
	Mod time.Time `json:"mod" rethinkdb:"mod"`

	// Hide is the time the document was hidden, or nil if not hidden.
	Hide *time.Time `json:"hide" rethinkdb:"hide,omitempty"`

	// Header may contain extra data needed for this document.
	Header `json:"header,omitempty" rethinkdb:"header,omitempty"`
}

// DocInfoGetter is an interface that returns the docinfo for a document.
type DocInfoGetter interface {
	// DocInfoGet returns the docinfo for this object.
	// May return nil to indicate it is not available.
	DocInfoGet() *DocInfo
}

// DocInfoSetter is an interface that sets the docinfo for a document.
type DocInfoSetter interface {
	// DocInfoSet returns the docinfo for this object.
	// Should return nil on success.
	// This function may optionally return an error if DocInfo is nil.
	DocInfoSet(*DocInfo) error
}

// DocInfoGetterSetter is an interface that gets and sets the docinfo for
// a document.
type DocInfoGetterSetter interface {
	DocInfoGetter
	DocInfoSetter
}

// DocInfoGet implements DocInfoGetter for DocInfo
func (di *DocInfo) DocInfoGet() *DocInfo {
	return di
}

// DocInfoSet implements DocInfoSetter for DocInfo
func (di *DocInfo) DocInfoSet(set *DocInfo) error {
	if di == nil || set == nil {
		return ErrorDocInfoSetNil
	}
	*di = *set
	return nil
}

// Returns wheter or not a user is allowed to perform the action indicated by
// mode with this document.
func (di DocInfo) Allowed(user User, needed Mode) bool {
	if user.Root {
		// Root can do anything and sees even hidden documents
		return true
	}
	// Hidden documents cannot be seen by anyone.
	if di.Hide != nil {
		return false
	}
	// Check if the user is one of the owners.
	mode, ok := di.Users[user.ID]
	if ok {
		return (needed & mode) == needed
	}

	// Sticky docs only look at user permissions if writing.
	if (needed&ModeWrite) == ModeWrite && (di.Mode&ModeSticky) == ModeSticky {
		return false
	}

	// Not an owner, check if the user is in one of the groups
	mode = 0
	inGroups := false
	for _, group := range user.Groups {
		groupMode, ok := di.Groups[group]
		if ok {
			inGroups = true
			mode |= groupMode
			// get additional mode bits from the user groups
		}
	}
	// Check the group permissions if we are in the group.
	if inGroups {
		// Admin users can bypass group permissions if they are part of the group.
		if user.Admin {
			return true
		}

		return (needed & mode) == needed
	}

	// Use the document mode as a default.
	return (needed & di.Mode) == needed
}
