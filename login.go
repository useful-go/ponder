package ponder

import (
	"golang.org/x/crypto/bcrypt"
)

// Creds is an interface that models credentials a user has to present
// to log in to the Ponder System.
// PasswordCreds are provided.
type Creds interface {
	// Creds returns the credentials data as a Header
	Creds() Header
}

// Login is an interface that models a method to login a user to the System.
// It also allows to update the credentials of the user.
// As a default a PasswordLogin that uses User.Hash is provided.
// Custom Login methods can use the User.Headers fields to store data.
type Login interface {
	// CheckLogin should atttempt to log in the user with the given creds.
	// It should return nil if the user logged in sucessfully or an error if not.
	CheckLogin(user User, creds Creds) error
	// Write should write the login for this user, possibly using the creds.
	WriteLogin(user *User, creds Creds) error
	// DeleteLogin should delete the login info for this user, but not the user
	// iself. Requires creds to confirm.
	DeleteLogin(user *User, cred Creds) error
}

// PasswordCreds are password credentials.
type PasswordCreds string

func (pwc PasswordCreds) Creds() Header {
	header := Header{}
	header.Set("password", string(pwc))
	return header
}

// PasswordLogin is a Login method that simply uses a bcrypt hash password
type PasswordLogin struct {
	*System
}

func (pwl PasswordLogin) CheckLogin(user User, creds Creds) error {
	credHeader := creds.Creds()
	password := credHeader.Get("password")
	hash := user.Header.Get("password-hash")
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
}

func (pwl PasswordLogin) WriteLogin(user *User, creds Creds) error {
	credHeader := creds.Creds()
	password := credHeader.Get("password")
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	if user.Header == nil {
		user.Header = Header{}
	}
	user.Header.Set("password-hash", string(hash))
	_, err = pwl.System.User.Put().Unchecked(user)
	return err
}

func (pwl PasswordLogin) DeleteLogin(user *User, creds Creds) error {
	if user == nil {
		return nil
	}
	err := pwl.CheckLogin(*user, creds)
	if err != nil {
		return err
	}
	user.Header.Del("password-hash")
	_, err = pwl.System.User.Put().Unchecked(user)
	return err
}
