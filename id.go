package ponder

import (
	"fmt"
	"net/url"
)

// ID is a universally unique identifier used to identify and link to
// various objects in Ponder.
// It can be anything, but it should be globally unique in the Ponder system.
// Therefore, a URL, UUID, or ULID, or similar could be used for this.
// However for User IDs or Group IDs is is often simpler to use a name.
type ID string

// NewDocID makes and URL that points to a document, using the ponder scheme.
// A document with id quux in database foo and table bar will be referred
// to as ponder://foo/bar#quux
func NewDocID(database, table, id string) ID {
	raw := fmt.Sprintf("ponder://%s/%s#%s", url.PathEscape(database), url.PathEscape(table), url.PathEscape(id))
	u, err := url.Parse(raw)
	if err != nil {
		panic(err)
	}
	return ID(u.String())
}
