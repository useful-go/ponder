/*
package ponder is a go language helper for RethinkDB.

Ponder implements a file-system like abstraction, with Rethinkdb tables
as directories and documents stored as files, with  Unix-like per document
permission system. It also contains various conveniences on top of the standard
driver.
*/
package ponder

import (
	"golang.org/x/net/idna"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
	"net/url"
	"path"
	"strings"
)

const defaultPrefix = FileName("zzz_ponder")
const docinfoName = FileName("docinfo")
const versionName = FileName("version")
const userName = FileName("user")
const groupName = FileName("group")

// SystemDir contains all Ponder specific Dirs
type SystemDir struct {
	// Doc dir
	Doc *Dir `json:"user" rethinkdb:"doc"`
	// User dir
	User *Dir `json:"doc" rethinkdb:"user"`
	// Group dir
	Group *Dir `json:"group" rethinkdb:"group"`
	// Version dir
	Version *Dir `json:"version" rethinkdb:"version"`
}

// System is a virtual file system and wrapper around a connection
// to a rethinkdb that has the convenience it might be mocked if needed.
// The methods of System do ignore the Ponder permission system,
// and all RethinkDB queries are executed zith the Rethinkdb privileges of
// the connected RethinkDB user.
// To uset he permission system use Login to obtain a Shell.
type System struct {
	// Name of the underlying database
	Name string `json:"name" rethinkdb:"name"`
	// QueryExecutor to wrap
	r.QueryExecutor `json:"-" rethinkdb:"-"`
	// Mock, if any
	Mock *r.Mock `json:"-" rethinkdb:"-"`
	// Session if any
	Session *r.Session `json:"-" rethinkdb:"-"`
	// Prefix to use for Ponder functionality, _ponder by default
	Prefix FileName `json:"prefix" rethinkdb:"prefix"`
	// SystemDir inherited.
	SystemDir `json:"system_dir" rethinkdb:"system_dir"`
	// The login to use. Set to PassordLogin by default.
	// Overwrite this to change the login method
	Login `json:"-" rethinkdb:"-"`
}

// Close closes the system.
func (s *System) Close() error {
	if s != nil && s.Session != nil {
		err := s.Session.Close()
		s.Session = nil
		return err
	}
	return nil
}

// Open opens a connection for a System and sets up Ponder on it.
// The urlstring should begin with rethinkdb:// for a real connection or
// rethinkdb+mock:// for a mock.
// The following options can be set in the query:
//  - ponder_prefix: prefix to use for ponder related tables, zzz_ponder default
func Open(urlString string) (*System, error) {
	sys, err := OpenWithoutPrepare(urlString)
	if err != nil || sys == nil {
		return sys, err
	}
	err = sys.Prepare()
	return sys, err
}

// OpenWithoutPrepare opens a connection for a System but does not set up Ponder
// on it. It does call Wait to wait util the database is ready.
func OpenWithoutPrepare(urlString string) (*System, error) {
	url, err := url.Parse(urlString)
	if err != nil {
		return nil, err
	}
	dbName := strings.Replace(strings.Trim(url.Path, "/"), "/", "-", -1)
	prefix := defaultPrefix
	if url.Query().Has("ponder_prefix") {
		prefix = FileName(url.Query().Get("ponder_prefix"))
	}

	if url.Scheme == "rethinkdb+mock" {
		mock := &r.Mock{}
		sys := &System{dbName, mock, mock, nil, prefix, SystemDir{}, nil}
		sys.Login = PasswordLogin{sys}
		return sys, nil
	}

	opts := r.ConnectOpts{Address: url.Host, Database: dbName}
	if url.User != nil {
		opts.Username = url.User.Username()
		opts.Password, _ = url.User.Password()
	}
	conn, err := r.Connect(opts)
	if err != nil {
		return nil, err
	}
	sys := &System{dbName, conn, nil, conn, prefix, SystemDir{}, nil}
	sys.Login = PasswordLogin{sys}
	sys.prepare(false) // only set up dirs

	err = sys.Wait()
	if err != nil {
		return nil, err
	}

	return sys, nil
}

// FileName is used to idernify Dir and dOc by Ponder.
// It is a string but it shiould be Punicode-encodable.
type FileName string

// String for FileName
func (fn FileName) String() string {
	return string(fn)
}

// Rethink is the name to use for Rethinkdb
func (fn FileName) Rethink() string {
	str, err := idna.ToASCII(strings.Replace(strings.Trim(string(fn), "/"), "/", "_", -1))
	if err != nil {
		panic(err)
	}
	return str
}

// Joins file names together.
func (fn FileName) Join(sub FileName) FileName {
	return FileName(path.Join(fn.String(), sub.String()))
}

// DirExists returns whether or not the dir exists in the drive.
func (s *System) DirExists(fn FileName) bool {
	query := r.TableList().Contains(fn.Rethink())
	res, err := query.Run(s)
	if err != nil {
		return false
	}
	var has bool
	res.One(&has)
	return has
}

// MkDir makes a dir with the given key, if any.
// Waits for the table to be fully created.
func (s *System) Mkdir(fn FileName, keys ...string) (*Dir, error) {
	opts := r.TableCreateOpts{}
	if len(keys) > 1 {
		opts.PrimaryKey = keys
	} else if len(keys) > 0 {
		opts.PrimaryKey = keys[0]
	}

	query := r.TableCreate(fn.Rethink(), opts)
	err := query.Exec(s)
	if err != nil {
		return nil, err
	}
	err = s.WaitForTable(fn.Rethink())
	if err != nil {
		return nil, err
	}
	return NewDir(s, fn, keys...), nil
}

// MkDir makes a dir with the given primary key, if any, if it doesn't exist.
// Returns the dir for further use unless if there was an error.
func (s *System) MkdirIfNew(fn FileName, pk ...string) (*Dir, error) {
	if !s.DirExists(fn) {
		return s.Mkdir(fn, pk...)
	}
	dir := NewDir(s, fn, pk...)
	return dir, nil
}

// ReadDir reads in a dir if exists
func (s *System) ReadDir(fn FileName) (*Dir, error) {
	if !s.DirExists(fn) {
		return nil, ErrorReadDirDoesNotExist
	}
	dir := NewDir(s, fn)

	res, err := dir.Table.Config().Field("primary_key").Run(dir)
	if err != nil {
		return nil, err
	}
	var pk string
	err = res.One(&pk)
	if err != nil {
		return nil, err
	}
	dir.PK = pk
	return dir, nil
}

// RmDir deletes a dir if exists, but only if it is empty.
func (s *System) RmDir(fn FileName) error {
	if !s.DirExists(fn) {
		return ErrorReadDirDoesNotExist
	}
	dir := NewDir(s, fn)
	res, err := dir.Table.Count().Run(dir)
	if err != nil {
		return err
	}
	var count int
	err = res.One(&count)
	if err != nil {
		return err
	}
	if count > 0 {
		return ErrorRmDirNotEmpty
	}

	return r.TableDrop(fn.Rethink()).Exec(s)
}

// Wait waits until the rethinkdb is completely ready.
func (s *System) Wait() error {
	return r.DB(s.Name).Wait().Exec(s)
}

// WaitForTable waits until the named rethinkdb table is completely ready.
func (s *System) WaitForTable(tableName string) error {
	return r.DB(s.Name).Table(tableName).Wait().Exec(s)
}

// Prepare prepares the System for use with Ponder.
func (s *System) Prepare() error {
	return s.prepare(true)
}

func (s *System) prepare(write bool) error {
	setups := map[FileName]**Dir{
		docinfoName: &s.Doc,
		versionName: &s.Version,
		userName:    &s.User,
		groupName:   &s.Group,
	}
	// set up the dirs
	for name, dirPtr := range setups {
		if write {
			dir, err := s.MkdirIfNew(s.Prefix.Join(name), "id")
			if err != nil {
				return err
			}
			*dirPtr = dir
		} else {
			*dirPtr = NewDir(s, s.Prefix.Join(name), "id")
		}
	}

	if !write {
		return nil
	}

	groups := []Group{
		{ID: "root", Link: NewDocID(s.Name, groupName.Rethink(), "root")},
		{ID: "admin", Link: NewDocID(s.Name, groupName.Rethink(), "admin")},
	}

	users := []User{
		{ID: "root", Link: NewDocID(s.Name, userName.Rethink(), "root"), Root: true, Admin: true},
		{ID: "admin", Link: NewDocID(s.Name, userName.Rethink(), "admin"), Admin: true},
	}
	// Set up the groups
	_, err := DirWriteAllUnchecked(s.Group.PutIfNew(), groups)
	if err != nil {
		return err
	}
	// Set up the users
	_, err = DirWriteAllUnchecked(s.User.PutIfNew(), users)
	if err != nil {
		return err
	}

	// users :=

	return nil
}

// PutDocInfo writes docinfo to the System's Doc Dir.
// The docinfo is used to determine the pernnissions of the document
// with the Id of info. Therefor it is neccerqy to use qgrlobqlly unique ID,
// such as a UUID, IRL or ULID.
func (s *System) PutDocInfo(info DocInfo) error {
	_, err := s.Doc.Put().Unchecked(info)
	return err
}

// GetDocInfo gets the docinfo for the given resource id.
// This can be used for permission checking or checking the modtime, etc.
func (s *System) GetDocInfo(id ID) (*DocInfo, error) {
	info := &DocInfo{}
	_, err := s.Doc.Get(id).Unchecked(info)
	return info, err
}

// GetDocInfos gets the docinfo for the given resource ids.
// This can be used for permission checking or checking the modtime, etc.
func (s *System) GetDocInfos(ids []ID) (*[]DocInfo, error) {
	info := []DocInfo{}
	return DirReadAllUnchecked(s.Group.GetMany(ArrayToAny(ids)...), &info)
}
