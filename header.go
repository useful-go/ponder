package ponder

import (
	"net/http"
)

// Header is used as a catch-all for storing supplemental data in Ponder.
// Reuse the http Header for this.
type Header = http.Header
