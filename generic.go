package ponder

// ArrayToAny converts an array of type Type to an array of any
func ArrayToAny[Type any](array []Type) []any {
	res := []any{}
	for _, v := range array {
		res = append(res, v)
	}
	return res
}

// ArrayToAny converts an array of any to an array of Type
func AnyToArray[Type any](array []any) []Type {
	res := []Type{}
	for _, v := range array {
		res = append(res, v.(Type))
	}
	return res
}

// StringsToStrings converts an array of string-likes a subtype of Type to an
// array of Type which must also be a sybtype of string.
func StringsToStrings[Type interface{ ~string }, Sub interface{ ~string }](array []Sub) []Type {
	res := []Type{}
	for _, v := range array {
		res = append(res, Type(v))
	}
	return res
}

func SystemPut[Type any](system *System, path FileName, object *Type) (*Type, error) {
	dir := NewDir(system, path)
	_, err := dir.Put().Unchecked(object)
	if err != nil {
		return nil, err
	}
	return object, nil
}

func SystemUpdate[Type any](system *System, path FileName, id ID, object *Type) (*Type, error) {
	dir := NewDir(system, path)
	_, err := dir.Update().Unchecked(object)
	if err != nil {
		return nil, err
	}
	return object, nil
}

func SystemGet[Type any](system *System, path FileName, id ID) (*Type, error) {
	var object *Type
	dir := NewDir(system, path)
	_, err := dir.Get(id).Unchecked(object)
	if err != nil {
		return nil, err
	}
	return object, nil
}

func SystemDelete(system *System, path FileName, id ID) error {
	dir := NewDir(system, path)
	return dir.Get(id).Delete()
}
