package ponder

type Error string

func (e Error) Error() string {
	return string(e)
}

const ErrorDocInfoSetNil = Error("DocInfoSet: cannot set nil document info")
const ErrorShellCannotLoadGroups = Error("Shell: cannot load user groups")
const ErrorReadDirDoesNotExist = Error("ReadDir: dir does not exist")
const ErrorRmDirNotEmpty = Error("RmDir: dir is not empty")
const ErrorCheckAccessDenied = Error("Check: access denied")
const ErrorPutAllIdsDoNotMatchObjects = Error("PutAll: IDs do not match objects")
