package ponder

// Group is a group who can have or not have access to a document in ponder.
// You can link this to the group in your own database using the UID.
// Ponder generates the "root" and the "nobody" groups on setup.
type Group struct {
	// ID is the group id. It can be anything but is must be globally unique
	ID ID `json:"id" rethinkdb:"id"`

	// Link is the link to your own group document.
	Link ID `json:"link" rethinkdb:"link"`

	// Header may contain extra data needed for this user.
	Header `json:"header,omitempty" rethinkdb:"header,omitempty"`

	// Custom is a field that can be used to store your own custom data
	Custom any `json:"custom,omitempty" rethinkdb:"custom,omitempty"`
}
