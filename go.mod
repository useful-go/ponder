module gitlab.com/useful-go/ponder

go 1.18

require (
	bitbucket.org/creachadair/shell v0.0.7 // indirect
	github.com/bitfield/script v0.20.2 // indirect
	github.com/chzyer/readline v1.5.1 // indirect
	github.com/golang/protobuf v1.3.4 // indirect
	github.com/hailocab/go-hostpool v0.0.0-20160125115350-e80d13ce29ed // indirect
	github.com/itchyny/gojq v0.12.7 // indirect
	github.com/itchyny/timefmt-go v0.1.3 // indirect
	github.com/matryer/is v1.4.0 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mattn/go-shellwords v1.0.12 // indirect
	github.com/opentracing/opentracing-go v1.1.0 // indirect
	github.com/peterh/liner v1.2.2 // indirect
	github.com/sirupsen/logrus v1.0.6 // indirect
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073 // indirect
	golang.org/x/exp v0.0.0-20220722155223-a9213eeb770e // indirect
	golang.org/x/net v0.0.0-20220812174116-3211cb980234 // indirect
	golang.org/x/sys v0.0.0-20220728004956-3c1f35247d10 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/cenkalti/backoff.v2 v2.2.1 // indirect
	gopkg.in/rethinkdb/rethinkdb-go.v6 v6.2.2 // indirect
)
