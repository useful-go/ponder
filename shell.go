package ponder

import (
	"fmt"
)

// Shell is a virtual shell session on a System that limits the permissions
// of the logged in User to what matches their permissions and rights.
type Shell struct {
	// System this shell is for
	*System `json:"system" rethinkdb:"system"`

	// User who is using the shell
	*User `json:"user" rethinkdb:"user"`

	// Groups of the user, cached
	Goups []Group `json:"groups" rethinkdb:"groups"`
}

// Shell starts a shell session in the System for the given Ponder user.
// Inside the shell the access is limited to what the user's permissions
// allow.
func (s *System) Shell(userID ID, creds Creds) (*Shell, error) {
	// Get user and log in.
	user := &User{}
	_, err := DirReadUnchecked(s.User.Get(userID), user)
	if err != nil {
		return nil, err
	}

	err = s.CheckLogin(*user, creds)
	if err != nil {
		return nil, err
	}

	// Cache groups for fast access.
	groups := []Group{}
	ids := ArrayToAny(user.Groups)
	_, err = DirReadAllUnchecked(s.Group.GetMany(ids...), &groups)
	if err != nil {
		// OK to error here, user must have at least one group!
		return nil, fmt.Errorf("%s: %w", ErrorShellCannotLoadGroups, err)
	}

	return &Shell{s, user, groups}, nil
}

// Returns a protected dir. Use this in addition to Dir.Check().
func (s *Shell) Dir(fn FileName, key ...string) *Dir {
	dir := NewDir(s.System, fn, key...)
	dir.Shell = s
	return dir
}
