package main

import (
	_ "encoding/json"
	"fmt"
	"github.com/mattn/go-shellwords"
	"github.com/peterh/liner"
	"gitlab.com/useful-go/ponder"
	_ "golang.org/x/exp/slices"
	"io"
	"os"
	"sort"
	"strings"
)

type shellFunc func(shell *ponder.Shell, args ...string) int
type shellCommand struct {
	shellFunc
	help string
}

func shellExit(shell *ponder.Shell, args ...string) int {
	return 4
}

func shellDirGet(shell *ponder.Shell, args ...string) int {
	if exit := printArgErr(3, args...); exit != 0 {
		return exit
	}
	dir := shell.Dir(ponder.FileName(args[1]))
	doc, err := dir.Get(args[2]).Check(ponder.ModeRead).UncheckedJSON()
	if err != nil {
		return printErr(args, 11, err)
	}
	fmt.Printf("%s\n", doc)
	return 0
}

func shellDirPut(shell *ponder.Shell, args ...string) int {
	var err error
	if exit := printArgErr(4, args...); exit != 0 {
		return exit
	}
	shellDir := shell.Dir(ponder.FileName(args[1]))
	id := ponder.ID(args[2])
	fname := args[3]
	var data []byte
	if fname == "-" {
		data, err = io.ReadAll(os.Stdin)
	} else {
		data, err = os.ReadFile(fname)
		if err != nil {
			return printErr(args, 12, err)
		}
	}

	obj, err := shellDir.Put().JSON(id, string(data))
	if err == nil {
		fmt.Printf("%s\n", obj)
	}
	return printErr(args, 14, err)
}

func shellDirRm(shell *ponder.Shell, args ...string) int {
	if exit := printArgErr(3, args...); exit != 0 {
		return exit
	}
	shellDir := shell.Dir(ponder.FileName(args[1]))
	id := ponder.ID(args[2])
	err := shellDir.Get(id).Delete()
	return printErr(args, 14, err)
}

func shellPrintf(shell *ponder.Shell, args ...string) int {
	if len(args) < 1 {
		return 1
	}
	vargs := ponder.ArrayToAny(args[1:])
	fmt.Fprintf(os.Stdout, args[0], vargs...)
	os.Stdout.Sync()
	return 0
}

func shellEcho(shell *ponder.Shell, args ...string) int {
	if len(args) < 1 {
		return 1
	}
	vargs := ponder.ArrayToAny(args[1:])
	fmt.Fprintln(os.Stdout, vargs...)
	os.Stdout.Sync()
	return 0
}

func shellHelp(shell *ponder.Shell, args ...string) int {
	keys := []string{}
	if len(args) > 1 {
		keys = args[1:]
	} else {
		for k, _ := range shellCommands {
			keys = append(keys, k)
		}
	}
	sort.Strings(keys)
	fmt.Fprintln(os.Stderr, "ponder shell commands:")
	for _, k := range keys {
		v, ok := shellCommands[k]
		if ok {
			parts := strings.SplitN(v.help, "\t", 2)
			helpArgs, helpText := parts[0], parts[1]
			fmt.Fprintf(os.Stderr, "\t%-16s %-16s\t%s\n", k, helpArgs, helpText)
		} else {
			fmt.Fprintf(os.Stderr, "\t%s %s\n", k, "unknown command")
		}
	}
	fmt.Fprintln(os.Stderr, "ponder shell applies user permissions to all commands.")
	return 1
}

func chainShellSystem() func(shell *ponder.Shell, args ...string) int {
	return func(shell *ponder.Shell, args ...string) int {
		if exit := printArgErr(2, args...); exit != 0 {
			return exit
		}
		sub := args[1]
		if cmd, ok := commands[sub]; ok {
			return cmd.commandFunc(shell.System, args[1:]...)
		} else {
			fmt.Fprintf(os.Stderr, "unknown system command %s\n", sub)
			return 1
		}
	}
}

var shellCommands map[string]shellCommand

func chainShell(name string) func(shell *ponder.Shell, args ...string) int {
	return func(shell *ponder.Shell, args ...string) int {
		if exit := printArgErr(2, args...); exit != 0 {
			return exit
		}
		sub := name + "." + args[0]
		if cmd, ok := shellCommands[sub]; ok {
			return cmd.shellFunc(shell, args[1:]...)
		} else {
			fmt.Fprintf(os.Stderr, "unknown shell command %s\n", sub)
			return 1
		}
	}
}

func shellDo(shell *ponder.Shell, env func(string) string, args ...string) int {
	if len(args) < 1 {
		return 0
	}
	cmd := args[0]
	if command, ok := shellCommands[cmd]; ok {
		return command.shellFunc(shell, args...)
	}
	fmt.Fprintf(os.Stderr, "Unknown command: %s\n", cmd)
	return 1
}

func interactiveShell(shell *ponder.Shell) int {
	vars := map[string]string{}
	in := liner.NewLiner()
	defer in.Close()

	in.SetCtrlCAborts(true)

	for {
		prompt := fmt.Sprintf("%s@%s> ", shell.User.ID, shell.System.Name)

		line, err := in.Prompt(prompt)
		if err != nil {
			return 0
		}

		getenv := func(name string) string {
			return vars[name]
		}

		line = strings.Trim(line, " \t\n\r")
		if len(line) < 1 {
			continue
		}
		in.AppendHistory(line)

		line = os.Expand(line, getenv)
		args, err := shellwords.Parse(line)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Syntax error: %s\n", err)
			continue
		}
		if len(args) > 2 && args[0] == "set" {
			vars[args[1]] = args[2]
		} else {
			res := shellDo(shell, getenv, args...)
			if res == 4 {
				return 0
			}
		}
	}

	return 0
}

func shell(system *ponder.System, args ...string) int {
	if exit := printArgErr(3, args...); exit != 0 {
		return exit
	}
	name := args[1]
	pass := args[2]
	shell, err := system.Shell(ponder.ID(name), ponder.PasswordCreds(pass))
	if err != nil {
		return printErr(args, 7, err)
	}
	printJson(shell)
	if len(args) > 3 {
		if args[3] == "-" {
			return interactiveShell(shell)
		} else {
			return shellDo(shell, os.Getenv, args[3:]...)
		}
	}
	return 0
}
