package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/useful-go/ponder"
	"golang.org/x/exp/slices"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
)

type commandFunc func(system *ponder.System, args ...string) int
type command struct {
	commandFunc
	help string
}

func printErr(args []string, exit int, err error) int {
	if err != nil {
		fmt.Fprintf(os.Stderr, "ponder %s %v error: %s\n", args[0], args[1:], err)
		return exit
	}
	return 0
}

func printArgErr(min int, args ...string) int {
	if len(args) < min {
		fmt.Fprintf(os.Stderr, "ponder %s error: expected at least %d arguments\n", args[0], min-1)
		return 8
	}
	return 0
}

func printJson(obj any) {
	buf, _ := json.Marshal(obj)
	fmt.Printf("%s\n", string(buf))
}

func prepare(system *ponder.System, args ...string) int {
	err := system.Prepare()
	return printErr(args, 1, err)
}

func dirMk(system *ponder.System, args ...string) int {
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	dir, err := system.Mkdir(ponder.FileName(args[1]), args[2:]...)
	if err == nil {
		printJson(dir)
	}
	return printErr(args, 6, err)
}

func dirInfo(system *ponder.System, args ...string) int {
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	dir, err := system.ReadDir(ponder.FileName(args[1]))
	if err == nil {
		printJson(dir)
	}
	return printErr(args, 6, err)
}

func dirGet(system *ponder.System, args ...string) int {
	if exit := printArgErr(3, args...); exit != 0 {
		return exit
	}
	dir, err := system.ReadDir(ponder.FileName(args[1]))
	if err != nil {
		return printErr(args, 11, err)
	}
	doc, err := dir.Get(args[2]).UncheckedJSON()
	if err != nil {
		return printErr(args, 11, err)
	}
	fmt.Printf("%s\n", doc)
	return 0
}

func dirPut(system *ponder.System, args ...string) int {
	var err error
	if exit := printArgErr(3, args...); exit != 0 {
		return exit
	}
	dir, err := system.ReadDir(ponder.FileName(args[1]))
	if err != nil {
		return printErr(args, 11, err)
	}
	fname := args[2]
	var data []byte
	if fname == "-" {
		data, err = io.ReadAll(os.Stdin)
	} else {
		data, err = os.ReadFile(fname)
		if err != nil {
			return printErr(args, 12, err)
		}
	}

	obj, err := dir.Put().UncheckedJSON(string(data))
	if err == nil {
		fmt.Printf("%s\n", obj)
	}
	return printErr(args, 14, err)
}

func dirDelete(system *ponder.System, args ...string) int {
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	err := system.RmDir(ponder.FileName(args[1]))
	return printErr(args, 6, err)
}

func docPut(system *ponder.System, args ...string) int {
	var err error
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	id := ponder.ID(args[1])

	info := ponder.DocInfo{}
	info.ID = id
	info.Mode = ponder.ModeRead | ponder.ModeList
	info.Mod = time.Now()

	i := 4
	for i < len(args) {
		kind := args[i-2]
		ugid := ponder.ID(args[i-1])
		val := args[i]
		modei, _ := strconv.ParseInt(val, 0, 64)
		mode := ponder.Mode(modei)
		i += 3
		if kind == "u" || kind == "user" {
			if info.Users == nil {
				info.Users = ponder.ModeMap{}
			}
			info.Users[ugid] = mode
		}
		if kind == "g" || kind == "group" {
			if info.Groups == nil {
				info.Groups = ponder.ModeMap{}
			}
			info.Groups[ugid] = mode
		}
		if kind == "m" || kind == "mode" {
			info.Mode = mode
		}
		if kind == "h" || kind == "head" {
			if info.Header == nil {
				info.Header = ponder.Header{}
			}
			info.Header.Set(string(ugid), val)
		}
		if kind == "H" || kind == "hide" {
			if val == "now" {
				now := time.Now()
				info.Hide = &now
			} else {
				tm, _ := time.Parse(time.RFC3339, val)
				info.Hide = &tm
			}
		}
	}

	err = system.PutDocInfo(info)
	return printErr(args, 14, err)
}

func docGet(system *ponder.System, args ...string) int {
	var err error
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	id := ponder.ID(args[1])
	info, err := system.GetDocInfo(id)
	if err != nil {
		printErr(args, 14, err)
	}
	printJson(info)
	return 0
}

func docGetMany(system *ponder.System, args ...string) int {
	var err error
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	infos, err := system.GetDocInfos(ponder.StringsToStrings[ponder.ID](args[1:]))
	if err != nil {
		printErr(args, 14, err)
	}
	printJson(*infos)
	return 0
}

func groupPut(system *ponder.System, args ...string) int {
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	link := ponder.ID("")
	if len(args) > 2 {
		link = ponder.ID(args[2])
	}
	group := &ponder.Group{ID: ponder.ID(args[1]), Link: link}
	res, err := system.Group.Put().Unchecked(group)
	if res != nil {
		printJson(res)
	}
	return printErr(args, 6, err)
}

func groupUpdate(system *ponder.System, args ...string) int {
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	group := ponder.Group{}
	_, err := system.Group.Get(args[1]).Unchecked(&group)
	if err != nil {
		return printErr(args, 9, err)
	}
	if len(args) > 2 {
		group.Link = ponder.ID(args[2])
	}
	res, err := system.Group.Update().Unchecked(group)
	if res != nil {
		printJson(res)
	}
	return printErr(args, 10, err)
}

func groupGet(system *ponder.System, args ...string) int {
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	group := &ponder.Group{}
	_, err := system.Group.Get(args[1]).Unchecked(group)
	if err == nil {
		printJson(group)
	}
	return printErr(args, 6, err)
}

func groupDelete(system *ponder.System, args ...string) int {
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	err := system.Group.Get(args[1]).DeleteUnchecked()
	return printErr(args, 6, err)
}

func groupGetMany(system *ponder.System, args ...string) int {
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	group := []ponder.Group{}
	ids := ponder.ArrayToAny(args[1:])
	_, err := ponder.DirReadAllUnchecked(system.Group.GetMany(ids...), &group)
	if err == nil {
		printJson(group)
	}
	return printErr(args, 6, err)
}

func groupGetAll(system *ponder.System, args ...string) int {
	if exit := printArgErr(1, args...); exit != 0 {
		return exit
	}
	group := []ponder.Group{}
	_, err := ponder.DirReadAllUnchecked(system.Group.All(), &group)
	if err == nil {
		printJson(group)
	}
	return printErr(args, 6, err)
}

func loginWrite(system *ponder.System, args ...string) int {
	if exit := printArgErr(3, args...); exit != 0 {
		return exit
	}
	pass := args[2]
	user := &ponder.User{}
	_, err := ponder.DirReadUnchecked(system.User.Get(args[1]), user)
	if err != nil {
		return printErr(args, 7, err)
	}

	err = system.WriteLogin(user, ponder.PasswordCreds(pass))
	if err != nil {
		return printErr(args, 7, err)
	}

	printJson(user)
	return 0
}

func loginCheck(system *ponder.System, args ...string) int {
	if exit := printArgErr(3, args...); exit != 0 {
		return exit
	}
	pass := args[2]
	user := &ponder.User{}
	_, err := ponder.DirReadUnchecked(system.User.Get(args[1]), user)
	if err != nil {
		return printErr(args, 7, err)
	}

	err = system.CheckLogin(*user, ponder.PasswordCreds(pass))
	if err != nil {
		return printErr(args, 7, err)
	}

	printJson(user)
	return 0
}

func loginDelete(system *ponder.System, args ...string) int {
	if exit := printArgErr(3, args...); exit != 0 {
		return exit
	}
	pass := args[2]
	user := &ponder.User{}
	_, err := ponder.DirReadUnchecked(system.User.Get(args[1]), user)
	printJson(user)
	if err != nil {
		return printErr(args, 7, err)
	}
	if user.ID == "" {
		fmt.Fprintln(os.Stderr, "No such user.")
		return 11
	}

	err = system.DeleteLogin(user, ponder.PasswordCreds(pass))
	if err != nil {
		return printErr(args, 7, err)
	}
	return 0
}

func userPut(system *ponder.System, args ...string) int {
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	link := ponder.ID("")
	if len(args) > 2 {
		link = ponder.ID(args[2])
	}
	user := &ponder.User{ID: ponder.ID(args[1]), Link: link}
	if len(args) > 3 {
		groups := args[3:]
		for _, group := range groups {
			if group == "root" {
				user.Root = true
			} else if group == "admin" {
				user.Admin = true
			}
			pgroup := ponder.ID(group)
			if !slices.Contains(user.Groups, pgroup) {
				user.Groups = append(user.Groups, pgroup)
			}
		}
	}

	res, err := system.User.Put().Unchecked(user)
	if user != nil {
		printJson(res)
	}
	return printErr(args, 6, err)
}

func userUpdate(system *ponder.System, args ...string) int {
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	user := &ponder.User{}
	_, err := ponder.DirReadUnchecked(system.User.Get(args[1]), user)
	if err != nil {
		return printErr(args, 8, err)
	}
	if len(args) > 2 {
		user.Link = ponder.ID(args[2])
	}
	if len(args) > 3 {
		groups := args[3:]
		for _, group := range groups {
			if group == "root" {
				user.Root = true
			} else if group == "admin" {
				user.Admin = true
			}
			pgroup := ponder.ID(group)
			if !slices.Contains(user.Groups, pgroup) {
				user.Groups = append(user.Groups, pgroup)
			}
		}
	}

	res, err := system.User.Update().Unchecked(user)
	if user != nil {
		printJson(res)
	}
	return printErr(args, 6, err)
}

func userGet(system *ponder.System, args ...string) int {
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	user := &ponder.User{}
	_, err := ponder.DirReadUnchecked(system.User.Get(args[1]), user)
	if err == nil {
		printJson(user)
	}
	return printErr(args, 6, err)
}

func userGetMany(system *ponder.System, args ...string) int {
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	user := []ponder.User{}
	ids := ponder.ArrayToAny(args[1:])
	_, err := system.User.GetMany(ids...).AllUnchecked(&user)
	if err == nil {
		printJson(user)
	}
	return printErr(args, 6, err)
}

func userGetAll(system *ponder.System, args ...string) int {
	if exit := printArgErr(1, args...); exit != 0 {
		return exit
	}
	user := []ponder.User{}
	_, err := system.User.All().AllUnchecked(&user)
	if err == nil {
		printJson(user)
	}
	return printErr(args, 6, err)
}

func userDelete(system *ponder.System, args ...string) int {
	if exit := printArgErr(2, args...); exit != 0 {
		return exit
	}
	user := &ponder.User{}
	_, err := ponder.DirReadUnchecked(system.User.Get(args[1]), user)
	if err != nil {
		return printErr(args, 6, err)
	}
	err = system.User.Get(args[1]).DeleteUnchecked()
	return printErr(args, 6, err)
}

var commands map[string]command

func help(system *ponder.System, args ...string) int {
	keys := []string{}
	if len(args) > 1 {
		keys = args[1:]
	} else {
		for k, _ := range commands {
			keys = append(keys, k)
		}
	}
	sort.Strings(keys)
	fmt.Fprintln(os.Stderr, "ponder usage: url command ... args")
	fmt.Fprintln(os.Stderr, "ponder is a command line tool to manage a Ponder system in a Rethinkdb database.")
	fmt.Fprintln(os.Stderr, "If PONDER_DB is set to the database UR, then the URL parameter can be omitted.")

	fmt.Fprintln(os.Stderr, "ponder commands:")
	for _, k := range keys {
		v, ok := commands[k]
		if ok {
			parts := strings.SplitN(v.help, "\t", 2)
			helpArgs, helpText := parts[0], parts[1]
			fmt.Fprintf(os.Stderr, "\t%-16s %-16s\t%s\n", k, helpArgs, helpText)
		} else {
			fmt.Fprintf(os.Stderr, "\t%s %s\n", k, "unknown command")
		}
	}
	fmt.Fprintln(os.Stderr, "")
	return 1
}

func chain(name string) func(system *ponder.System, args ...string) int {
	return func(system *ponder.System, args ...string) int {
		if exit := printArgErr(2, args...); exit != 0 {
			return exit
		}
		sub := name + "." + args[1]
		if cmd, ok := commands[sub]; ok {
			return cmd.commandFunc(system, args[1:]...)
		} else {
			fmt.Fprintf(os.Stderr, "ponder: unknown command %s\n", sub)
			return 1
		}
	}
}

func main() {
	commands = map[string]command{
		"dir":      {chain("dir"), "<sub> [args*] [link]\tCall command dir.<sub>"},
		"dir.mk":   {dirMk, "<name> [key]\tMake dir, optionally with the given primary key name."},
		"dir.info": {dirInfo, "<name>\tGet dir info."},
		"dir.get":  {dirGet, "<name> <id>\tGet document from dir."},
		"dir.put":  {dirPut, "<name> <file.json>\tPut JSON file into dir. Read from stdin if -."},
		"dir.rm":   {dirDelete, "<name>\tDelete dir, destroying all documents in it."},

		"doc":      {chain("doc"), "<sub> [args*] [link]\tCall command doc.<sub>"},
		"doc.put":  {docPut, "<id>[type k v]+\tPut docinfo for document with the given id."},
		"doc.get":  {docGet, "<id>\tGet docinfo with id."},
		"doc.many": {docGetMany, "<id+>\tGet docinfo with ids."},

		"group":        {chain("group"), "<sub> [args*] [link]\tCall command group.<sub>"},
		"group.put":    {groupPut, "<id> [link]\tAdd or overwrite group with id and link."},
		"group.all":    {groupGetAll, "[no arguments]\tGet all groups."},
		"group.rm":     {groupDelete, "<id>\tDelete group with id."},
		"group.get":    {groupGet, "<id>\tGet group with id."},
		"group.many":   {groupGetMany, "<id+>\tGet groups with ids."},
		"group.update": {groupUpdate, "<id> [link]\tUpdate group with link."},

		"help": {help, "[topic]\tShow help."},

		"login":       {chain("login"), "<sub> [args*] [link]\tCall command login.<sub>"},
		"login.write": {loginWrite, "<user_id> [password]\tSet login password for the user."},
		"login.check": {loginCheck, "<user_id> [password]\tCheck login password for the user."},
		"login.rm":    {loginDelete, "<user_id> [password]\tDelete login password, requires password for check."},

		"prepare": {prepare, "[no arguments]\tPrepare to use ponder on the database."},

		"shell": {shell, "<user_id> <password> [commands]\tRun a shell for the user. Interactive if command is '-' ."},

		"user":        {chain("user"), "<sub> [args*] [link]\tCall command user.<sub>"},
		"user.put":    {userPut, "<id> [link] [group*]\tAdd or overwrite user with id, link and groups."},
		"user.all":    {userGetAll, "[no arguments]\tGet all users."},
		"user.rm":     {userDelete, "<id>\tDelete user with id."},
		"user.get":    {userGet, "<id>\tGet user with id."},
		"user.many":   {userGetMany, "<id+>\tGet users with ids."},
		"user.update": {userUpdate, "<id> [link] [group*]\tUpdate user with link and groups."},
	}

	shellCommands = map[string]shellCommand{
		"help":    {shellHelp, "[topic]\tShow shell help."},
		"dir":     {chainShell("dir"), "<sub> [args*] [link]\tCall command dir.<sub>"},
		"dir.get": {shellDirGet, "<name> <id>\tGet doc."},
		"dir.put": {shellDirPut, "<name> <id> <file.json>\tPut JSON file into dir. Read from stdin if -."},
		"dir.rm":  {shellDirRm, "<name> <id>\tRemove doc."},
		"exit":    {shellExit, "[no arguments]\tExit shell."},
		"echo":    {shellEcho, "[args]\tEcho args."},
		"printf":  {shellPrintf, "fmt [args]\tCall printf."},
		"system":  {chainShellSystem(), "system cmd [args]\tCall system command."},
	}

	needed := 3

	dbUrl, ok := os.LookupEnv("PONDER_DB")
	var system *ponder.System
	var err error
	if ok {
		system, err = ponder.OpenWithoutPrepare(dbUrl)
		if err != nil {
			fmt.Fprintf(os.Stderr, "ponder: error: %s\n", err)
			os.Exit(2)
		}
		defer system.Close()
		needed = 2
	}

	if len(os.Args) < needed {
		help(nil)
		return
	}

	if !ok {
		system, err = ponder.OpenWithoutPrepare(os.Args[1])
		if err != nil {
			fmt.Fprintf(os.Stderr, "ponder: error: %s\n", err)
			os.Exit(2)
		}
		defer system.Close()
	}
	shift := needed - 1
	if cmd, ok := commands[os.Args[shift]]; ok {
		os.Exit(cmd.commandFunc(system, os.Args[shift:]...))
	} else {
		fmt.Fprintf(os.Stderr, "ponder: unknown command %s\n", os.Args[2])
		return
	}
}
