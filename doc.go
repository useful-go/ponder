package ponder

// Doc is a document stored with Ponder which has to be checked for access
// with the ponder permission system.
type Doc interface {
	// Must be able to get/set documents
	DocInfoGetterSetter
}

// Docs is an array of documents.
type Docs []Doc
