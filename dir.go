package ponder

import (
	"golang.org/x/exp/maps"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

// DocInfoField is the field used to store docinfo in
const DocInfoField = "_ponder_docinfo"

// Dir is a virtual directory with documents, mapped to a rethinkdb table.
type Dir struct {
	// system is the underlying system, inherited
	*System `json:"-" rethinkdb:"-"`

	// Virtual filename of this Dir
	FileName `json:"file_name" rethinkdb:"file_name"`

	// PK is the name of the primary key that will be used for this dir
	// If empty, Ponder uses "id".
	PK string `json:"pk,omitempty" rethinkdb:"pk,omitempty"`

	// Table is the table expression for this dir
	Table r.Term `json:"-" rethinkdb:"-"`

	// Shell that is using this dir. May be null if the dir is being used
	// by the System, and no shell is connected
	Shell *Shell `json:"-" rethinkdb:"-"`
}

// NewDir return a new dir but does NOT save it.
func NewDir(sys *System, fn FileName, pk ...string) *Dir {
	tableName := fn.Rethink()
	table := r.Table(tableName)
	pkname := ""
	if len(pk) > 0 {
		pkname = pk[0]
	}
	return &Dir{sys, fn, pkname, table, nil}
}

// DirReader allows to specify read conditions easily
type DirReader struct {
	// Dir this was derived from.
	*Dir `json:"-" rethinkdb:"-"`
	// Condition for reading.
	Condition r.Term `json:"-" rethinkdb:"-"`
	// DocInfo is the docinfo expression for this DirReader
	DocInfo r.Term `json:"-" rethinkdb:"-"`
	// Multiple is true if this reader is for reading multiple objects
	Multiple bool
}

// DirWriter allows to specify options on writing easily.
type DirWriter struct {
	// Dir this was derived from.
	*Dir `json:"-" rethinkdb:"-"`
	// Condition for writing.
	Condition r.Term `json:"-" rethinkdb:"-"`
	// Options for inserting
	r.InsertOpts `json:"-" rethinkdb:"-"`
	// DocInfo is the docinfo expression for this DirWriter
	DocInfo r.Term `json:"-" rethinkdb:"-"`
}

func (d Dir) DocTable() r.Term {
	if d.System.Doc != nil {
		return d.System.Doc.Table
	}
	return r.Table(d.System.Prefix + docinfoName)
}

// Get returns a DirReader for reading a single document by ID.
func (d *Dir) Get(key any) *DirReader {
	return &DirReader{d, d.Table.Get(key), d.DocTable().Get(key), false}
}

// GetMany returns a DirReader for reading multiple documents by ID.
func (d *Dir) GetMany(keys ...any) *DirReader {
	return &DirReader{d, d.Table.GetAll(keys...), d.DocTable().GetAll(keys...), true}
}

// GetAll returns a DirReader for reading multiple documents.
// It will return all documents.
func (d *Dir) All() *DirReader {
	docinfoIds := d.Table.Filter(true).Field(d.PK)
	return &DirReader{d, d.Table.Filter(true), d.DocTable().GetAll(docinfoIds), true}
}

// TermGetDocInfo gets docinfo from a Term using an executor.
func TermGetDocInfo(qe r.QueryExecutor, docInfo r.Term) ([]DocInfo, error) {
	res, err := docInfo.Run(qe)
	if err != nil {
		return nil, err
	}
	infos := []DocInfo{}
	err = res.All(&infos)
	if err != nil {
		return nil, err
	}
	return infos, nil
}

// CheckDocInfos return a list of DocInfo IDs for which the check is OK,
// and one for which it is not ok.
func CheckDocInfos(user User, needed Mode, infos []DocInfo) ([]ID, []ID) {
	allowed := []ID{}
	notAllowed := []ID{}
	for _, info := range infos {
		if info.Allowed(user, needed) {
			allowed = append(allowed, info.ID)
		} else {
			notAllowed = append(notAllowed, info.ID)
		}
	}
	return allowed, notAllowed
}

// Check fetches the docinfo using the dockinfo term and checks if the user
// has access with the needed mode. The docinfo selection should select
// the matching docinfo for the documents to load.
// Multiple should be set to trtue if the goal is to check multiple documents
// in stead of one.
// The function returns a term, based on table, that that will use
// Get or GetAll on table to return the documents the user is allowed to see.
// If the user cannot see any documents an error term with
// ErrorCheckAccessDenied is returned in stead.
// An error term is also returned if the fetchuing of the docinfo fails somehow.
func CheckUserDocInfo(user User, needed Mode, qe r.QueryExecutor, multiple bool, docInfo r.Term, table r.Term) r.Term {
	infos, err := TermGetDocInfo(qe, docInfo)
	if err != nil {
		return r.Error(err)
	}

	allowed, _ := CheckDocInfos(user, needed, infos)

	if multiple {
		return table.GetAll(ArrayToAny(allowed)...)
	} else if len(allowed) > 0 {
		return table.Get(allowed[0])
	} else {
		return r.Error(ErrorCheckAccessDenied)
	}
}

// Check fetches the docinfo and checks if the user of Dir.Shell has access
// with the needed mode. If Dir.Shell is nil, this does nothing.
// The reader modified to become a getAll which only includes the documents to
// which access is allowed, possibly ending up with an empty list.
func (dr *DirReader) Check(needed Mode) *DirReader {
	if dr.Dir.Shell == nil {
		return dr
	}

	user := dr.Dir.Shell.User
	if user == nil {
		return dr
	}

	checked := CheckUserDocInfo(*user, needed, dr, dr.Multiple, dr.DocInfo, dr.Table)
	dr.Condition = checked
	return dr
}

// Unchecked gets a single object from the dir with given ID.
// This is a low level function that does not do any permission checks.
// Will return an error if no results were found.
func (dr *DirReader) Unchecked(object any) (any, error) {
	res, err := dr.Condition.Run(dr)
	if err != nil {
		return nil, err
	}
	err = res.One(object)
	return object, err
}

// UncheckedJSON gets a single object from the dir with given ID.
// The object is converted to a JSON string.
// This is a low level function that does not do any permission checks.
// Will return an error if no results were found.
func (dr *DirReader) UncheckedJSON() (string, error) {
	res, err := dr.Condition.ToJSON().Run(dr)
	if err != nil {
		return "", err
	}
	var jsonStr string
	err = res.One(&jsonStr)
	return jsonStr, err
}

// AllUnchecked gets an array of objects from the dir with given ID.
// This is a low level function that does not do any permission checks.
// Will return an error if no results were found.
func (dr *DirReader) AllUnchecked(array any) (any, error) {
	res, err := dr.Condition.Run(dr)
	if err != nil {
		return nil, err
	}
	err = res.All(array)
	return array, err
}

// Read gets a single object from the dir with given ID.
// This function calls Check first with ModeRead.
// Will return an error if no results were found.
func (dr *DirReader) Read(object any) (any, error) {
	dr = dr.Check(ModeRead)
	res, err := dr.Condition.Run(dr)
	if err != nil {
		return nil, err
	}
	err = res.One(object)
	return object, err
}

// ReadJSON gets a single object from the dir with given ID.
// The object is converted to a JSON string.
// This function calls Check first with ModeRead.
// Will return an error if no results were found.
func (dr *DirReader) ReadJSON() (string, error) {
	dr = dr.Check(ModeRead)
	res, err := dr.Condition.ToJSON().Run(dr)
	if err != nil {
		return "", err
	}
	var jsonStr string
	err = res.One(&jsonStr)
	return jsonStr, err
}

// AllRead gets an array of objects from the dir with given ID.
// This function calls Check first with ModeRead.
// Will return an error if no results were found.
func (dr *DirReader) ReadAll(array any) (any, error) {
	dr = dr.Check(ModeRead)
	res, err := dr.Condition.Run(dr)
	if err != nil {
		return nil, err
	}
	err = res.All(array)
	return array, err
}

// DeleteUnchecked deletes all matches for the DirReader.
// This is a low level function that does not do any permission checks.
func (dr *DirReader) DeleteUnchecked() error {
	res, err := dr.Condition.Delete().RunWrite(dr)
	if err == nil && res.Errors > 0 {
		return Error(res.FirstError)
	}
	return err
}

// Delete deletes all matches for the DirReader.
// This function calls Check first with ModeWrite.
func (dr *DirReader) Delete() error {
	dr = dr.Check(ModeWrite)
	res, err := dr.Condition.Delete().RunWrite(dr)
	if err == nil && res.Errors > 0 {
		return Error(res.FirstError)
	}
	return err
}

// Put returns a DirWriter that will overwrite existing records.
func (d *Dir) Put() *DirWriter {
	return &DirWriter{d, d.Table.Filter(true), r.InsertOpts{Conflict: "replace"}, d.DocTable()}
}

// Update returns a DirWriter that will update/merge existing records.
func (d *Dir) Update() *DirWriter {
	return &DirWriter{d, d.Table.Filter(true), r.InsertOpts{Conflict: "update"}, d.DocTable()}
}

// Put returns a DirWriter that will NOT overwrite existing records,
// and only write new records.
func (d *Dir) PutIfNew() *DirWriter {
	return &DirWriter{d, d.Table.Filter(true), r.InsertOpts{Conflict: func(id, oldDoc, newDoc any) any {
		return oldDoc
	}}, d.DocTable()}
}

// Check returns an error if the write is blocked by the permissions.
// If the ID does not exist in the docinfo, then it is considered that the
// document is new, and that docinfo must be generated for it, since writing
// a new doc is always allowed.
// If Dir.Shell is nil, this does nothing.
func (dw *DirWriter) Check(needed Mode, objects map[ID]any) ([]DocInfo, error) {
	if dw.Dir.Shell == nil {
		return nil, nil
	}

	user := dw.Dir.Shell.User
	if user == nil {
		return nil, nil
	}
	newObjects := map[ID]any{}
	maps.Copy(newObjects, objects)
	ids := maps.Keys(objects)
	newInfos := []DocInfo{}
	oldInfos, err := TermGetDocInfo(dw, dw.System.Doc.Table.GetAll(ArrayToAny(ids)...))
	if err != nil {
		return nil, err
	}
	for _, oldInfo := range oldInfos {
		if !oldInfo.Allowed(*user, needed) {
			return nil, ErrorCheckAccessDenied
		}
		// all which are old, remove them
		delete(newObjects, oldInfo.ID)
	}

	// now objectCopy is only new objects, perpare new docinfos for them
	// By default the user and their own group get read write, and
	// te other groups they are in get read
	for id, _ := range newObjects {
		info := DocInfo{ID: id}
		info.Users = map[ID]Mode{user.ID: ModeRead | ModeWrite}
		info.Groups = map[ID]Mode{user.ID: ModeRead | ModeWrite}
		for _, group := range user.Groups {
			if group != user.ID {
				info.Groups[group] = ModeRead
			}
		}
		newInfos = append(newInfos, info)
	}

	return newInfos, err
}

// Unchecked writes a single object.
// This is a low level function that does not do any permission checks.
func (dw DirWriter) Unchecked(object any) (any, error) {
	query := dw.Table.Insert(object, dw.InsertOpts)
	err := query.Exec(dw)
	if err != nil {
		return nil, err
	}
	return object, nil
}

// Unchecked writes a single JSON string.
// This is a low level function that does not do any permission checks.
func (dw DirWriter) UncheckedJSON(jsonStr string) (any, error) {
	query := dw.Table.Insert(r.JSON(jsonStr), dw.InsertOpts)
	err := query.Exec(dw)
	if err != nil {
		return nil, err
	}
	return jsonStr, nil
}

// Unchecked writes an array.
// This is a low level function that does not do any permission checks.
func (dw DirWriter) AllUnchecked(array []any) ([]any, error) {
	query := dw.Table.Insert(array, dw.InsertOpts)
	err := query.Exec(dw)
	if err != nil {
		return nil, err
	}
	return array, nil
}

// One writes a single object.
// This function calls Check first with ModeWrite.
// It also creates Docinfo for any new documents.
func (dw DirWriter) One(id ID, object any) (any, error) {
	newInfos, err := dw.Check(ModeWrite, map[ID]any{id: object})
	if err != nil {
		return nil, err
	}
	query := dw.Table.Insert(object, dw.InsertOpts)
	err = query.Exec(dw)

	if err != nil {
		return nil, err
	}
	err = dw.DocInfo.Insert(newInfos, dw.InsertOpts).Exec(dw)
	if err != nil {
		return nil, err
	}
	return object, nil
}

// JSOM writes a single JSON string.
// This function calls Check first with ModeWrite.
// It also creates Docinfo for any new documents.
func (dw DirWriter) JSON(id ID, jsonStr string) (any, error) {
	newInfos, err := dw.Check(ModeWrite, map[ID]any{id: jsonStr})
	if err != nil {
		return nil, err
	}
	query := dw.Table.Insert(r.JSON(jsonStr), dw.InsertOpts)
	err = query.Exec(dw)
	if err != nil {
		return nil, err
	}
	err = dw.DocInfo.Insert(newInfos, dw.InsertOpts).Exec(dw)
	if err != nil {
		return nil, err
	}
	return jsonStr, nil
}

// All writes an array of objects by ID.
// The IDs are needed to be able to check the permissions.
// This function calls Check first with ModeWrite.
// It also creates Docinfo for any new documents.
func (dw DirWriter) All(objects map[ID]any) ([]any, error) {
	newInfos, err := dw.Check(ModeWrite, objects)
	if err != nil {
		return nil, err
	}
	array := maps.Values(objects)
	query := dw.Table.Insert(array, dw.InsertOpts)
	err = query.Exec(dw)
	if err != nil {
		return nil, err
	}
	err = dw.DocInfo.Insert(newInfos, dw.InsertOpts).Exec(dw)
	if err != nil {
		return nil, err
	}
	return array, nil
}

// DirWriteUnchecked generically stores a Ponder object or document to the dir.
// This is a low level function that does not do any permission checks.
// If the object exists it will be overwritten.
func DirWriteUnchecked[Type any](dw *DirWriter, object Type) (Type, error) {
	o, err := dw.Unchecked(object)
	return o.(Type), err
}

// DirWriteAllUnchecked generically stores an array of Ponder objecst or document
// to the dir.
// This is a low level function that does not do any permission checks.
// If the object exists it will be overwritten.
func DirWriteAllUnchecked[Type any](dw *DirWriter, array []Type) ([]Type, error) {
	objects := []any{}
	for _, elt := range array {
		objects = append(objects, elt)
	}
	o, err := dw.AllUnchecked(objects)
	res := []Type{}
	for _, elt := range o {
		res = append(res, elt.(Type))
	}
	return res, err
}

// DirReadUnchecked generically stores a Ponder object or document to the dir.
// This is a low level function that does not do any permission checks.
// If the object exists it will be overwritten.
func DirReadUnchecked[Type any](dr *DirReader, object Type) (Type, error) {
	o, err := dr.Unchecked(object)
	if err != nil {
		var zero Type
		return zero, err
	}
	return o.(Type), err
}

// DirReadAllUnchecked generically stores an array of Ponder objecst or document
// to the dir.
// This is a low level function that does not do any permission checks.
// If the object exists it will be overwritten.
func DirReadAllUnchecked[Type any](dw *DirReader, array *[]Type) (*[]Type, error) {
	o, err := dw.AllUnchecked(array)
	return o.(*[]Type), err
}
