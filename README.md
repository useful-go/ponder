# Ponder

# Introduction

Ponder is a go language helper for RethinkDB.

Ponder implements a file-system like abstraction, with Rethinkdb tables
as directories and documents stored as files, with  Unix-like per document
permission system. It also contains various conveniences on top of the standard
driver.

# Using Ponder as a library

Start with Open(url) to create a System, then you can work directly on
the System for convenient usage without permission. Or, for restricted access,
use System.Shell() to log in to the ponder system.

To overwrite the standard login method, set System.Login to your own login
method.

To store data that the Shell can work with, each document should use a globally
unique ID over all entries in the database. ID such as a UUID, a ULID, or a
URL are required, because ponder tracks the document info and permissions of
all documents in a single table with this ID.

Go doc documentation is available, use it to understand ponder better.

# Command line tool

In cmd/ponder a command line tool is available to administer the ponder
permission system on the command line.

The tool is also a good example of how to use Ponder as a library.
It is also a good way to test the Ponder library itself.

Use ponder help for more information.

# Changes

v0.1.4: Admin users can bypass group permissions if they are part of that group.
