package ponder

import "os"
import "strings"
import "testing"
import "github.com/matryer/is"
import "github.com/bitfield/script"

const envDBURL = "PONDER_DB"
const defaultDBURL = "rethinkdb://localhost:28015/test"

func helperStartDocker(t *testing.T) string {
	t.Helper()
	url := defaultDBURL
	err := script.Exec(`docker run -d -p 8080:8080 -p 28015:28015 --name rethinkdb  rethinkdb`).Error()
	if err != nil {
		t.Fatalf("Docker error: %s\n", err)
		return url
	}

	t.Cleanup(func() {
		script.Exec(`docker stop rethinkdb`).Wait()
		script.Exec(`docker rm rethinkdb`).Wait()
	})

	addr, err := script.Exec(`docker inspect --format '{{ .NetworkSettings.IPAddress }}' rethinkdb`).String()
	if err == nil {
		url = "rethinkdb://" + strings.Trim(addr, "\n") + ":28015/test"
	} else {
		t.Fatalf("Docker error: %s\n", err)
		return url
	}
	return url
}

func helperTestDBURL(t *testing.T) string {
	t.Helper()
	url := defaultDBURL
	addr, err := script.Exec(`docker inspect --format '{{ .NetworkSettings.IPAddress }}' rethinkdb`).String()
	if err == nil {
		url = "rethinkdb://" + strings.Trim(addr, "\n") + ":28015/test"
	} else {
		t.Logf("Docker error: %s\n", err)
	}
	if found, ok := os.LookupEnv(envDBURL); ok {
		url = found
	}
	t.Logf("Database: %s\n", url)
	return url
}

func TestOpen(t *testing.T) {
	is := is.New(t)
	url := helperTestDBURL(t)
	sys, err := Open(url)
	is.NoErr(err)
	is.True(sys != nil)
	is.Equal(sys.Name, "test")
	err = sys.Close()
	is.NoErr(err)
}

func TestOpenWithWrite(t *testing.T) {
	is := is.New(t)
	url := helperTestDBURL(t)
	sys, err := Open(url)
	is.NoErr(err)
	is.True(sys != nil)
	is.Equal(sys.Name, "test")
	defer sys.Close()
	user := &User{ID: ID("user_id")}
	_, err = SystemPut(sys, sys.User.FileName, user)
	is.NoErr(err)
	err = SystemDelete(sys, sys.User.FileName, user.ID)
	is.NoErr(err)
}
