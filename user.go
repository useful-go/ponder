package ponder

// User is a user who can have or not have access to a document in Ponder.
// You can link this to the user in your own database using the UID and
// the Authericator interface. This is different from the RethinkDB user.
// Ponder generates the "root" and the "nobody" users on setup.
type User struct {
	// ID is the ID of the user. It can be anything but it must be globally
	// unique. It is also used as the login name of this user.
	ID ID `json:"id" rethinkdb:"id"`

	// Groups are the IDs of the groups to which this user belongs.
	Groups []ID `json:"groups" rethinkdb:"groups"`

	// Link is the link to your own user document.
	Link ID `json:"link" rethinkdb:"link"`

	// Header may contain extra data needed for this user.
	Header `json:"header,omitempty" rethinkdb:"header,omitempty"`

	// Root is true if this is a root user.
	// Root users can bypass all permissions for all Ponder documents.
	Root bool `json:"root" rethinkdb:"root"`

	// Admin is true if this is an admin user.
	// Admin users can bypass permissions for Ponder documents that
	// belong to one of the groups the admin is in, but not for
	// documents that they are not in the group for.
	Admin bool `json:"admin" rethinkdb:"admin"`

	// Login is the Login linked to the ponder User
	Login `json:"-" rethinkdb:"-"`

	// Custom is a field that can be used to store your own custom data
	Custom any `json:"custom,omitempty" rethinkdb:"custom,omitempty"`
}
